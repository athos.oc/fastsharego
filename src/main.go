package main

import (
	"fmt"
	"log"
	"net"
	"os"
	"strings"

	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
)

var rangoIp string = "192.168."

func main() {

	if len(os.Args) > 1 && os.Args[1] == "all-interfaces" {
		rangoIp = ""
	}

	dir, err := os.Getwd()
	if err != nil {
		log.Fatal(err)
	}

	ifaces, err := net.Interfaces()

	for _, i := range ifaces {
		addrs, _ := i.Addrs()

		for _, addr := range addrs {
			var ip net.IP
			switch v := addr.(type) {
			case *net.IPNet:
				ip = v.IP
				ipStr := ip.String()

				if rangoIp != "" {

					if esNuestraRed := strings.Contains(ipStr, rangoIp); esNuestraRed == true {
						fmt.Printf("http://%v:1323\n", ip)
					}

				} else {
					fmt.Printf("http://%v:1323\n", ip)
				}
			}
		}
	}

	e := echo.New()
	e.Use(middleware.StaticWithConfig(middleware.StaticConfig{
		Root:   dir,
		Browse: true,
	}))

	e.HideBanner = true
	e.Logger.Fatal(e.Start("0.0.0.0:1323"))

}
